package com.api_rest.carros.app.domain;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
public class Carros {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CARROS")
    @SequenceGenerator(name = "SEQ_CARROS", sequenceName = "SEQ_CARROS", allocationSize = 1)
    private Long id;

    @Column(name = "NOME")
    private String nome;

    @Column(name = "DESCRICAO")
    private String descricao;

    @Column(name = "URL_FOTO")
    private String urlFoto;

    @Column(name = "URL_VIDEO")
    private String urlVideo;

    @Column(name = "LATITUDE")
    private String latitude;

    @Column(name = "LONGITUDE")
    private String longitude;

    @Column(name = "TIPO")
    private String tipo;

}
