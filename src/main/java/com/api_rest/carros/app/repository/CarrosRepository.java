package com.api_rest.carros.app.repository;

import com.api_rest.carros.app.domain.Carros;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CarrosRepository extends JpaRepository<Carros, Long>, JpaSpecificationExecutor<Carros> {

}
