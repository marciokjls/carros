connect system/oracle as sysdba
alter session set "_ORACLE_SCRIPT"=true;
--**********************************
--Esquema carros
--**********************************

create tablespace carros datafile '/opt/oracle/oradata/carros01.dbf' size 400M online;
create tablespace idx_carros datafile '/opt/oracle/oradata/idx_carros01.dbf' size 400M;
create user carros identified by carros default tablespace carros temporary tablespace temp;
alter user carros quota unlimited on carros;
grant resource to carros;
grant connect to carros;
grant create view to carros;
grant create job to carros;
grant create procedure to carros;
grant create materialized view to carros;
alter user carros default role connect, resource;

exit;
