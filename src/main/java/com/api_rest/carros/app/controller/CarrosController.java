package com.api_rest.carros.app.controller;

import com.api_rest.carros.app.domain.Carros;
import com.api_rest.carros.app.repository.CarrosRepository;
import com.api_rest.carros.app.service.CarrosService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/")
public class CarrosController {

    private final CarrosService carrosService;

    @GetMapping
    public ResponseEntity<Page<Carros>> findAll(Pageable pageable) {
        return ResponseEntity.status(HttpStatus.OK).body(carrosService.findAll(pageable));
    }

        @GetMapping("/{id}")
    public ResponseEntity<Carros> findbyId(@PathVariable() Long id){
        return ResponseEntity.status(HttpStatus.OK).body(carrosService.findById(id));
    }
    @GetMapping("/tipo/{tipo}")
    public ResponseEntity<Page<Carros>> findByTipo(@PathVariable() String tipo, Pageable pageable) {
        return ResponseEntity.status(HttpStatus.OK).body(carrosService.findBytipo(tipo, pageable));
    }

    @PostMapping()
    public ResponseEntity<Carros> save(@RequestBody Carros Carro) {
        var carro = carrosService.save(Carro);
        return ResponseEntity.status(HttpStatus.CREATED).body(carro);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Carros> update(@PathVariable() Long id, @RequestBody Carros updateCarro) {
        var carro = carrosService.update(id, updateCarro);
        return ResponseEntity.status(HttpStatus.OK).body(carro);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable() Long id) {
        carrosService.delete(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }


}
