package com.api_rest.carros.app.specification;

import com.api_rest.carros.app.domain.Carros;
import org.springframework.data.jpa.domain.Specification;

public class CarrosSpecification {
    public static Specification<Carros> byTipo(String tipo) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.like(root.get("tipo"), "%" + tipo + "%");
    }

    public static Specification<Carros> byId(Long id) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("id"), id);
    }
}
