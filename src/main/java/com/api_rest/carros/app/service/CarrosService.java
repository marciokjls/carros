package com.api_rest.carros.app.service;

import com.api_rest.carros.app.domain.Carros;
import com.api_rest.carros.app.repository.CarrosRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static com.api_rest.carros.app.specification.CarrosSpecification.byTipo;

@Service
@AllArgsConstructor
public class CarrosService {
    private final CarrosRepository carrosRepository;

    public Page<Carros> findAll(Pageable pageable) {
        Optional<Page<Carros>> carrosOptional = Optional.of(carrosRepository.findAll(pageable));
        return carrosOptional.orElseThrow(() -> new EntityNotFoundException("Nenhum carro encontrado"));
    }

    public Carros findById(Long id) {
        var carro = carrosRepository.findById(id);
        return carro.orElseThrow(() -> new EntityNotFoundException("Carro não encontrado com id " + id));
    }

    public Carros save(Carros carro) {
        return carrosRepository.save(carro);
    }

    public void delete(Long id) {
        var carro = carrosRepository
                .findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Carro não encontrado com id " + id));
        carrosRepository.delete(carro);
    }

    public Carros update(Long id, Carros UpdateCarro) {
        var carro = carrosRepository
                .findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Carro não encontrado com id " + id));
        BeanUtils.copyProperties(UpdateCarro, carro);
        return carrosRepository.save(carro);
    }

    public Page<Carros> findBytipo(String tipo, Pageable pageable) {
        return carrosRepository.findAll(byTipo(tipo), pageable);
    }

}
